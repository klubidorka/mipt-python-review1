import argparse
import json
import random


def args_read():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', dest='model', required=True, help='Путь к файлу, в который сохраняется модель.')
    parser.add_argument('--length', dest='length', required=True, help='Длина генерирумого текста.')
    parser.add_argument('--seed', dest='seed', help='Начальное слово.')
    parser.add_argument('--output', dest='output', help='Путь к файлу, в который сохраняется текст.')
    args = parser.parse_args()
    return args


def main(args):
    f = open(args.model)
    model = json.load(f)
    if args.seed is not None and args.seed in model:
        output_text = args.seed
    else:
        output_text = random.choice(list(model.keys()))  # Первое слово текста
    curr_word = output_text

    for i in range(int(args.length) - 1):
        arr = []
        for next_word, amount in model[curr_word].items():
            arr.extend([next_word] * amount)
        if curr_word in model[curr_word].keys():
            next_word = random.choice(arr)
            output_text = output_text + " " + next_word

        else:
            next_word = random.choice(list(model.keys()))
            output_text = output_text + " " + next_word
        curr_word = next_word
    if args.output is not None:
        out = open(args.output, 'w')
        out.write(output_text + ".")
    else:
        print(output_text + ".")


if __name__ == '__main__':
    args_ = args_read()
    main(args_)
