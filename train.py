import argparse
import json
import os.path
import sys
from collections import defaultdict


# Очистка от неалфавитных символов
def cleaner(word):
    container = list(word)
    word = ''
    container = list(filter(lambda x: x.isalpha(), container))
    word = word.join(container)
    return word


def text_reader(input_stream, model, args):
    prev_word = ''
    for line in input_stream:
        for word in line.split():
            if args.lc is True:
                word = word.lower()
            word = cleaner(word)
            if word and prev_word:
                model[prev_word][word] += 1
            prev_word = word
    return model


def args_read():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input-dir', dest='dir', help='Путь к директории с текстами для считывания.')
    parser.add_argument('--model', dest='model', required=True, help='Путь к файлу, в который сохраняется модель.')
    parser.add_argument('--lc', dest='lc', action='store_true', help='Приведение текста к нижнему регистру.')

    args = parser.parse_args()
    return args


def main(args):
    path_m = args.model
    model = defaultdict(lambda: defaultdict(int))  # распределение слов
    if args.dir is None:
        model = text_reader(sys.stdin, model, args)
    else:
        file_list = os.listdir(path=args.dir)
        for file in file_list:
            input_stream = open(os.path.join(args.dir, file))
            model = text_reader(input_stream, model, args)
    f = open(path_m, 'w')
    f.write(json.dumps(model))
    f.close()


if __name__ == '__main__':
    args_ = args_read()
    main(args_)
